import 'package:column_widget_example/profilePage.dart';
import 'package:flutter/material.dart';

import 'mainPage.dart';

class Setting extends StatelessWidget {
  const Setting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('มหาวิทยาลัยบูรพา'),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return MainPage();
                })
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return ProfilePage();
                })
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.calendar_month),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return Setting();
                })
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }
}
