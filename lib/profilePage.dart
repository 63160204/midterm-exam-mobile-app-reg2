
import 'package:column_widget_example/profilePage.dart';
import 'package:column_widget_example/schedule.dart';
import 'package:column_widget_example/setting.dart';
import 'package:flutter/material.dart';

import 'mainPage.dart';


class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('มหาวิทยาลัยบูรพา'),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return MainPage();
                })
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.person),
              onPressed: () {}
            ),
            IconButton(
              icon: Icon(Icons.calendar_month),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return Schedule();
                })
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context){
                  return Setting();
                })
                );
              },
            ),
          ],
        ),
      ),
      body: Container(padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('โปรไฟล์', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            SizedBox(height: 10),
            Card(
              child: Row(
                children: [
                  Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage("https://scontent.fbkk12-3.fna.fbcdn.net/v/t1.6435-9/151777122_1377840489219161_197593444705939836_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeGHJj3jMP3_CDjkVnqSxKpzOagCN-Cb6es5qAI34Jvp6-OxoXsna3RbdtLSssojO7jw9pfFAPb3guoGMx5CRssJ&_nc_ohc=06It_Vp-OlkAX8hoYti&tn=VhtPJD1QDlPTDbB8&_nc_ht=scontent.fbkk12-3.fna&oh=00_AfAPbAKdE2eQoFZqhDoEDw6KE1XEtEcIKqUJZe9vV8f2Pg&oe=6401D2EB"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("ปฐมวรรณ แหลมผึ้ง", style: TextStyle(fontSize: 16)),
                      Text("คณะวิทยาการสารสนเทศ", style: TextStyle(fontSize: 14, color: Colors.grey)),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 25),
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("ข้อมูลส่วนบุลคล", style: TextStyle(fontSize: 20)),
                  Text("ชื่อ นางสาวปฐมวรรณ แหลมผึ้ง", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("ว/ด/ป  29 ตุลาคม 2544", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("เพศ หญิง", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("สัญชาติ ไทย", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("รหัสประจำตัว 63160204", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("คณะ วิทการสารสนเทศ", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("สาขา วิทยาการคอมพิมเตอร์", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("อีเมล 63160204@go.buu.ac.th", style: TextStyle(fontSize: 14, color: Colors.grey)),
                  Text("เบอร์โทรติดต่อ 092-876xxxx", style: TextStyle(fontSize: 14, color: Colors.grey)),
                ],
              ),
              ),
          ],
        ),
      ),
    );
  }
}


