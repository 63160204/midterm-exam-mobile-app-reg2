import 'package:flutter/material.dart';

import 'mainPage.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            //const SizedBox(height: 50.0),
            const Spacer(flex: 5),
            //const FlutterLogo(size: 100.0),
            Container(
                width: 200,
                height: 200,
                child: Image.network("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/903px-Buu-logo11.png?20221010124315")
            ),
           // const SizedBox(height: 100.0),
            const Spacer(flex: 10),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(flex: 3),
          //  const SizedBox(height: 30.0),
            ElevatedButton(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return MainPage();
              })
              );
            }, child: const Text('Login')),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
